<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Proadd extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
         if (!$this->session->userdata('is_admin_login')) {
            redirect('admin/home');
        }
	else if ($this->session->userdata('user_type')=="AG"){ redirect('admin/check');}
    }

    public function index() {
        $arr['page'] = 'proadd';
        $this->load->view('admin/proadd',$arr);
    }
    
    
    

}

/* End of file welcome.php */
/* Location: ./application/controllers/admin/proadd.php */
