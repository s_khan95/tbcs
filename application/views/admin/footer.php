<footer class="footer">
            <div class="container-fluid">
                
                <div class="copyright pull-right">
                    &copy; <script>document.write(new Date().getFullYear())</script><a href="http://www.digitallycans.com">Digital Lycans</a>
                </div>
            </div>
        </footer>
<!--   Core JS Files   -->
    
    <script src="<?php echo HTTP_JS_PATH; ?>jquery-1.10.2.js" type="text/javascript"></script>
    <script src="<?php echo HTTP_JS_PATH; ?>bootstrap.min.js" type="text/javascript"></script>
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>-->
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>

<!--     <script src="http://52.15.219.52/epromote/js/jquery.form-validator.min.js"></script>
 -->    
    <script src="<?php echo HTTP_JS_PATH; ?>nprogress.js"></script>




    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script src="<?php echo HTTP_JS_PATH; ?>ajax.js"></script>
    <script src="<?php echo HTTP_JS_PATH; ?>valid.js"></script>

    <!--  Checkbox, Radio & Switch Plugins -->
    <script src="<?php echo HTTP_JS_PATH; ?>bootstrap-checkbox-radio.js"></script>

    <!--  Charts Plugin -->
    <script src="<?php echo HTTP_JS_PATH; ?>chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="<?php echo HTTP_JS_PATH; ?>bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>

    <!-- Paper Dashboard Core javascript and methods for Demo purpose -->
    <script src="<?php echo HTTP_JS_PATH; ?>paper-dashboard.js"></script>

    <!-- Paper Dashboard DEMO methods, don't include it in your project! -->
    <script src="<?php echo HTTP_JS_PATH; ?>demo.js"></script>

    <script src= "https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src= "https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
    <script src= "//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src= "//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script src= "//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script src= "//cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js "></script>
