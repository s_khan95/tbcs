<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>TBCS - redundency checker application ver 1.0</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <!-- CSS Files -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/css/now-ui-kit.css?v=1.1.0" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/css/demo.css" rel="stylesheet" />
    <style>
    #cover{
        position:absolute;
        top:10%;
        left:42%;
    }
    .login-page .card-login{
    heignt:450px;
    }
#cover::before {
    width: 400px;
    height: 550px;
    content: "";
    position: absolute;
    top: -25px;
    left: -25px;
    bottom: 0;
    right: 0;
    background: inherit;
    box-shadow: inset 0 0 0 200px rgba(255,255,255,0.2);
    filter: blur(10px);
}

.radio label::before, .radio label::after{
border: 1px solid #234;
}
.input-group-addon.icon{
border-left:1px solid #234 !important;
border-top:1px solid #234 !important;
border-bottom:1px solid #234 !important;

}
#username,#pass{
color:#234;
border-right:1px solid #234;
border-top:1px solid #234;
border-bottom:1px solid #234;
}

.now-ui-icons{
color:#234;
}



.login-page .card-login.card-plain .form-control::-moz-placeholder {
    color: #234;
    }
</style>
<script>
            $(function() {

                if (localStorage.chkbx && localStorage.chkbx != '') {
                    $('#remember_me').attr('checked', 'checked');
                    $('#username').val(localStorage.usrname);
                    $('#pass').val(localStorage.pass);
                } else {
                    $('#remember_me').removeAttr('checked');
                    $('#username').val('');
                    $('#pass').val('');
                }

                $('#remember_me').click(function() {

                    if ($('#remember_me').is(':checked')) {
                        // save username and password
                        localStorage.usrname = $('#username').val();
                        localStorage.pass = $('#pass').val();
                        localStorage.chkbx = $('#remember_me').val();
                    } else {
                        localStorage.usrname = '';
                        localStorage.pass = '';
                        localStorage.chkbx = '';
                    }
                });
            });

        </script>
</head>

<body class="login-page sidebar-collapse">
    <div class="page-header" filter-color="">
        <div class="page-header-image" style="background-image:url(<?php echo base_url(); ?>assets/img/tbcs_bg.jpg)"></div>
                <div class="card card-login card-plain container" id="cover">
                    <form class="form" method="post" action="<?php echo base_url(); ?>admin/home/do_login">
                        <div class="header header-primary text-center">
				<img src="<?php echo base_url(); ?>assets/img/tbcs-logo.png" height="auto" width="100%">
				<center>Redundency Check System</center>
                            <div class="logo-container">
                                
                            </div>
                        </div>
                        <div class="content">
				<?php
        			if(isset($error) && $error !='')
        			{
            			?>
        				<div class="alert alert-danger">
        				<?php echo $error; ?>
      					</div>
        			<?php
        			}
        			?>
				<div class="input-group form-group-no-border input-lg">
			    	<div class="radio">
				    <input type="radio" name="user_type" id="tl" value="SA">
				    <label for="tl" style="margin-right:8em">
					Manager
				    </label>
				    <input type="radio" name="user_type" id="agent" value="AG" checked="">
				    <label for="agent">
					Agent
				    </label>
				</div>
                            </div>
                            <div class="input-group form-group-no-border input-lg">
                                <span class="input-group-addon icon">
                                    <i class="now-ui-icons users_circle-08"></i>
                                </span>
                                <input type="text" class="form-control" placeholder="User Name" name="username" id="username" autofocus>
                            </div>
                            <div class="input-group form-group-no-border input-lg">
                                <span class="input-group-addon icon">
                                    <i class="now-ui-icons text_caps-small"></i>
                                </span>
                                <input type="password" placeholder="Password" class="form-control" type="password" id="pass" name="password"/>
                            </div>
                            <div class="input-group form-group-no-border input-lg">
                                 <button type="submit" href="#pablo" class="btn btn-primary btn-round btn-lg btn-block">Login Now</button>
                                 </div>
                            
                        </div>
                        <div class="footer text-center">
                        </div>
                        
                    </form>
                </div>
        <footer class="footer">
            <div class="container">
                
                <div class="copyright">
                    &copy;
                    <script>
                        document.write(new Date().getFullYear())
                    </script>, Designed by
                    <a href="http://www.digitallycans.com" target="_blank">Digital Lycans</a>
                </div>
            </div>
        </footer>
    </div>
</body>
<!--   Core JS Files   -->
<script src="<?php echo base_url(); ?>assets/js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/core/popper.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/core/bootstrap.min.js" type="text/javascript"></script>
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="<?php echo base_url(); ?>assets/js/plugins/bootstrap-switch.js"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="<?php echo base_url(); ?>assets/js/plugins/nouislider.min.js" type="text/javascript"></script>
<!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
<script src="<?php echo base_url(); ?>assets/js/plugins/bootstrap-datepicker.js" type="text/javascript"></script>
<!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
<script src="<?php echo base_url(); ?>assets/js/now-ui-kit.js?v=1.1.0" type="text/javascript"></script>

</html>


